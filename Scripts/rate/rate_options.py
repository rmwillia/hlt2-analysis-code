###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Use the hlt2_big_minbias.py options file to give input file LFNS
# e.g. MooreAnalysis/run gaudirun.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_big_minbias.py <path/to/this/file>

from Moore import options
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
import os

options.ntuple_file+="_rate.root"


options.evt_max = 500_000 # 1_000_000
options.input_type = 'ROOT'
options.data_type = 'Upgrade'

options.input_raw_format = 4.3
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.simulation = True

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=2)

with reconstruction.bind(from_file=True):
    run_moore_with_tuples(
        options, False, public_tools=[stateProvider_with_simplified_geom()])
#














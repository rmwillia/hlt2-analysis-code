from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bp}[B+ =>  ${mup}mu+ ${mum}mu- ${Kp}K+ ]CC"
)
options.input_files = [
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000011_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000018_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000019_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000023_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000026_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000027_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000032_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000035_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000037_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000038_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000040_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000041_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000042_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000046_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000047_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000049_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000052_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000053_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000055_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000056_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000057_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000060_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000061_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000062_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000064_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000066_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000067_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000069_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000070_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000071_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000072_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000073_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000079_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000085_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000088_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000091_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000092_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000095_1.xdigi",
"root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154106/0000/00154106_00000097_1.xdigi",
]


options.input_type = 'ROOT'
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
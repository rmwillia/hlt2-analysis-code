from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bp}[B+ =>(J/psi(1S) =>  ${ep}e+ ${em}e-)  ${pip}pi+ ]CC"
)

files = [
"00143573_00000001_1.xdigi",
"00143573_00000002_1.xdigi",
"00143573_00000003_1.xdigi",
"00143573_00000005_1.xdigi",
"00143573_00000014_1.xdigi",
"00143573_00000019_1.xdigi",
"00143573_00000024_1.xdigi",
"00143573_00000025_1.xdigi",
"00143573_00000027_1.xdigi",
"00143573_00000029_1.xdigi",
"00143573_00000030_1.xdigi",
"00143573_00000032_1.xdigi",
"00143573_00000033_1.xdigi",
"00143573_00000034_1.xdigi",
"00143573_00000035_1.xdigi",
"00143573_00000038_1.xdigi",
"00143573_00000039_1.xdigi",
"00143573_00000040_1.xdigi",
"00143573_00000041_1.xdigi",
"00143573_00000043_1.xdigi",
"00143573_00000044_1.xdigi",
"00143573_00000045_1.xdigi",
"00143573_00000046_1.xdigi",
"00143573_00000047_1.xdigi",
"00143573_00000049_1.xdigi",
"00143573_00000050_1.xdigi",
"00143573_00000051_1.xdigi",
"00143573_00000052_1.xdigi",
"00143573_00000053_1.xdigi",
"00143573_00000055_1.xdigi",
"00143573_00000057_1.xdigi",
"00143573_00000058_1.xdigi",
"00143573_00000059_1.xdigi",
"00143573_00000060_1.xdigi",
"00143573_00000062_1.xdigi",
"00143573_00000063_1.xdigi",
"00143573_00000065_1.xdigi",
"00143573_00000068_1.xdigi",
"00143573_00000072_1.xdigi",
"00143573_00000073_1.xdigi",
"00143573_00000075_1.xdigi",
"00143573_00000077_1.xdigi",
"00143573_00000083_1.xdigi",
"00143573_00000084_1.xdigi",
"00143573_00000085_1.xdigi",
"00143573_00000086_1.xdigi",
"00143573_00000087_1.xdigi",
"00143573_00000090_1.xdigi",
"00143573_00000091_1.xdigi",
"00143573_00000094_1.xdigi",
"00143573_00000095_1.xdigi",
"00143573_00000096_1.xdigi",
"00143573_00000097_1.xdigi",
"00143573_00000098_1.xdigi",
"00143573_00000099_1.xdigi",
"00143573_00000105_1.xdigi",
"00143573_00000106_1.xdigi",
]
#12153020
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143573/0000/" + file
    file_places.append( file_place )
    
options.input_files = file_places
options.input_type = 'ROOT'
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'



# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()])   
    
    
    
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${B0}[B0 => (rho(770)0 => ${pip}pi+ ${pim}pi- ) (J/psi(1S) =>  ${mup}mu+ ${mum}mu-) ]CC"
)

files = [
"00076714_00000002_1.ldst",
"00076714_00000003_1.ldst",
"00076714_00000004_1.ldst",
"00076714_00000005_1.ldst",
"00076714_00000006_1.ldst",
"00076714_00000007_1.ldst",
"00076714_00000008_1.ldst",
"00076714_00000039_1.ldst",
"00076714_00000040_1.ldst",
"00076714_00000041_1.ldst",
"00076714_00000056_1.ldst",
"00076714_00000057_1.ldst",
"00076714_00000058_1.ldst",
"00076714_00000073_1.ldst",
"00076714_00000085_1.ldst",
]
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076714/0000/" + file
    file_places.append( file_place )

options.input_files = file_places
options.input_type = "ROOT"
options.input_raw_format = 4.3

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=2)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
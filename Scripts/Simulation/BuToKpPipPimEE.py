from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bu}[B+ => (K_1(1270)+ => ${Kp}K+ ${pim}pi- ${pip}pi+) ${ep}e+ ${em}e- ]CC"
)

files = [
"00154104_00000008_1.xdigi",
"00154104_00000015_1.xdigi",
"00154104_00000020_1.xdigi",
"00154104_00000024_1.xdigi",
"00154104_00000026_1.xdigi",
"00154104_00000030_1.xdigi",
"00154104_00000032_1.xdigi",
"00154104_00000033_1.xdigi",
"00154104_00000037_1.xdigi",
"00154104_00000040_1.xdigi",
"00154104_00000043_1.xdigi",
"00154104_00000044_1.xdigi",
"00154104_00000048_1.xdigi",
"00154104_00000049_1.xdigi",
"00154104_00000052_1.xdigi",
"00154104_00000053_1.xdigi",
"00154104_00000054_1.xdigi",
"00154104_00000056_1.xdigi",
"00154104_00000059_1.xdigi",
"00154104_00000061_1.xdigi",
"00154104_00000062_1.xdigi",
"00154104_00000063_1.xdigi",
"00154104_00000065_1.xdigi",
"00154104_00000067_1.xdigi",
"00154104_00000069_1.xdigi",
"00154104_00000070_1.xdigi",
"00154104_00000071_1.xdigi",
"00154104_00000072_1.xdigi",
"00154104_00000075_1.xdigi",
"00154104_00000077_1.xdigi",
"00154104_00000083_1.xdigi",
"00154104_00000089_1.xdigi",
"00154104_00000091_1.xdigi",
"00154104_00000092_1.xdigi",
"00154104_00000094_1.xdigi",
"00154104_00000096_1.xdigi",
"00154104_00000102_1.xdigi",

]
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154104/0000/" + file
    file_places.append( file_place )

options.input_files = file_places
options.input_type = "ROOT"
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=4)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
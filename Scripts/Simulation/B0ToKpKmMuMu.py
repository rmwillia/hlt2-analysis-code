from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bs}[B_s0 => ( phi(1020) => ${Kp}K+ ${Km}K- ) ${mup}mu+ ${mum}mu- ]CC"
)

files = [
"00154102_00000011_1.xdigi",
"00154102_00000016_1.xdigi",
"00154102_00000018_1.xdigi",
"00154102_00000020_1.xdigi",
"00154102_00000023_1.xdigi",
"00154102_00000028_1.xdigi",
"00154102_00000031_1.xdigi",
"00154102_00000032_1.xdigi",
"00154102_00000034_1.xdigi",
"00154102_00000036_1.xdigi",
"00154102_00000038_1.xdigi",
"00154102_00000041_1.xdigi",
"00154102_00000043_1.xdigi",
"00154102_00000044_1.xdigi",
"00154102_00000047_1.xdigi",
"00154102_00000050_1.xdigi",
"00154102_00000051_1.xdigi",
"00154102_00000053_1.xdigi",
"00154102_00000055_1.xdigi",
"00154102_00000056_1.xdigi",
"00154102_00000058_1.xdigi",
"00154102_00000059_1.xdigi",
"00154102_00000061_1.xdigi",
"00154102_00000063_1.xdigi",
"00154102_00000064_1.xdigi",
"00154102_00000065_1.xdigi",
"00154102_00000069_1.xdigi",
"00154102_00000072_1.xdigi",
"00154102_00000075_1.xdigi",
"00154102_00000079_1.xdigi",
"00154102_00000081_1.xdigi",
"00154102_00000084_1.xdigi",
"00154102_00000088_1.xdigi",
"00154102_00000089_1.xdigi",
"00154102_00000090_1.xdigi",
"00154102_00000092_1.xdigi",
"00154102_00000093_1.xdigi",
"00154102_00000095_1.xdigi",
]
#/MC/Upgrade/13124006
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154102/0000/" + file
    file_places.append( file_place )
    
options.input_files = file_places
options.input_type = 'ROOT'
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
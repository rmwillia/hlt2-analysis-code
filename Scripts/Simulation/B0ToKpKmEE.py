from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bs}[B_s0 => ( phi(1020) => ${Kp}K+ ${Km}K- ) ${ep}e+ ${em}e- ]CC"
)

files = [
"00109332_00000001_1.xdst",
"00109332_00000002_1.xdst",
"00109332_00000003_1.xdst",
"00109332_00000004_1.xdst",
"00109332_00000005_1.xdst",
"00109332_00000006_1.xdst",
"00109332_00000007_1.xdst",
"00109332_00000008_1.xdst",
"00109332_00000009_1.xdst",
"00109332_00000010_1.xdst",
"00109332_00000011_1.xdst",
"00109332_00000012_1.xdst",
"00109332_00000013_1.xdst",
"00109332_00000014_1.xdst",
"00109332_00000015_1.xdst",
"00109332_00000016_1.xdst",
"00109332_00000017_1.xdst",
"00109332_00000018_1.xdst",
"00109332_00000019_1.xdst",
"00109332_00000020_1.xdst",
"00109332_00000021_1.xdst",
"00109332_00000022_1.xdst",
"00109332_00000023_1.xdst",
"00109332_00000024_1.xdst",
"00109332_00000025_1.xdst",
"00109332_00000026_1.xdst",
"00109332_00000027_1.xdst",
"00109332_00000028_1.xdst",
"00109332_00000029_1.xdst",
"00109332_00000030_1.xdst",
"00109332_00000031_1.xdst",
"00109332_00000032_1.xdst",
"00109332_00000033_1.xdst",
"00109332_00000034_1.xdst",
"00109332_00000035_1.xdst",
"00109332_00000036_1.xdst",
"00109332_00000037_1.xdst",
"00109332_00000038_1.xdst",
"00109332_00000039_1.xdst",
"00109332_00000040_1.xdst",
"00109332_00000041_1.xdst",
"00109332_00000042_1.xdst",
"00109332_00000043_1.xdst",
"00109332_00000044_1.xdst",
"00109332_00000045_1.xdst",
"00109332_00000046_1.xdst",
"00109332_00000047_1.xdst",
"00109332_00000048_1.xdst",
"00109332_00000049_1.xdst",
"00109332_00000050_1.xdst",
"00109332_00000051_1.xdst",
"00109332_00000052_1.xdst",
"00109332_00000053_1.xdst",
"00109332_00000054_1.xdst",
"00109332_00000055_1.xdst",
"00109332_00000056_1.xdst",
"00109332_00000057_1.xdst",
"00109332_00000058_1.xdst",
"00109332_00000059_1.xdst",
"00109332_00000060_1.xdst",
"00109332_00000061_1.xdst",
"00109332_00000062_1.xdst",
"00109332_00000063_1.xdst",
"00109332_00000064_1.xdst",
"00109332_00000065_1.xdst",
"00109332_00000066_1.xdst",
"00109332_00000067_1.xdst",
"00109332_00000068_1.xdst",
"00109332_00000069_1.xdst",
"00109332_00000070_1.xdst",
"00109332_00000071_1.xdst",
"00109332_00000072_1.xdst",
"00109332_00000073_1.xdst",
"00109332_00000074_1.xdst",
"00109332_00000075_1.xdst",
"00109332_00000076_1.xdst",
"00109332_00000077_1.xdst",
"00109332_00000078_1.xdst",
"00109332_00000079_1.xdst",
"00109332_00000080_1.xdst",
"00109332_00000095_1.xdst",
"00109332_00000096_1.xdst",
]
#/MC/Upgrade/13124006
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00109332/0000/" + file
    file_places.append( file_place )
    
options.input_files = file_places
options.input_type = 'ROOT'
options.input_raw_format = 4.3

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=4)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
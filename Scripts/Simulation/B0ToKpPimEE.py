from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bd}[B0 => (K*(892)0 => ${Kp}K+ ${Pim}pi- ) ${ep}e+ ${em}e- ]CC"
)

files = [
"00154110_00000009_1.xdigi",
"00154110_00000018_1.xdigi",
"00154110_00000020_1.xdigi",
"00154110_00000023_1.xdigi",
"00154110_00000024_1.xdigi",
"00154110_00000025_1.xdigi",
"00154110_00000026_1.xdigi",
"00154110_00000031_1.xdigi",
"00154110_00000035_1.xdigi",
"00154110_00000038_1.xdigi",
"00154110_00000043_1.xdigi",
"00154110_00000045_1.xdigi",
"00154110_00000046_1.xdigi",
"00154110_00000048_1.xdigi",
"00154110_00000051_1.xdigi",
"00154110_00000053_1.xdigi",
"00154110_00000055_1.xdigi",
"00154110_00000058_1.xdigi",
"00154110_00000060_1.xdigi",
"00154110_00000061_1.xdigi",
"00154110_00000062_1.xdigi",
"00154110_00000064_1.xdigi",
"00154110_00000065_1.xdigi",
"00154110_00000066_1.xdigi",
"00154110_00000067_1.xdigi",
"00154110_00000069_1.xdigi",
"00154110_00000071_1.xdigi",
"00154110_00000072_1.xdigi",
"00154110_00000074_1.xdigi",
"00154110_00000077_1.xdigi",
"00154110_00000080_1.xdigi",
"00154110_00000087_1.xdigi",
"00154110_00000088_1.xdigi",
"00154110_00000090_1.xdigi",
"00154110_00000091_1.xdigi",
"00154110_00000098_1.xdigi",
]
#12153020
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154110/0000/" + file
    file_places.append( file_place )
    
options.input_files = file_places
options.input_type = 'ROOT'
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
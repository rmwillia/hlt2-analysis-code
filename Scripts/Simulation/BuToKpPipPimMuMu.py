from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_node as allen_sequence
from RecoConf.reconstruction_objects import reconstruction



options.ntuple_file+="_eff.root" ### DONT CHANGE

decay = (
    "${Bu}[B+ => (K_1(1270)+ => ${Kp}K+ ${pim}pi- ${pip}pi+) ${mup}mu+ ${mum}mu- ]CC"
)

files = [
"00154100_00000012_1.xdigi",
"00154100_00000016_1.xdigi",
"00154100_00000018_1.xdigi",
"00154100_00000021_1.xdigi",
"00154100_00000023_1.xdigi",
"00154100_00000026_1.xdigi",
"00154100_00000028_1.xdigi",
"00154100_00000031_1.xdigi",
"00154100_00000032_1.xdigi",
"00154100_00000035_1.xdigi",
"00154100_00000036_1.xdigi",
"00154100_00000037_1.xdigi",
"00154100_00000041_1.xdigi",
"00154100_00000042_1.xdigi",
"00154100_00000045_1.xdigi",
"00154100_00000047_1.xdigi",
"00154100_00000048_1.xdigi",
"00154100_00000049_1.xdigi",
"00154100_00000050_1.xdigi",
"00154100_00000054_1.xdigi",
"00154100_00000055_1.xdigi",
"00154100_00000056_1.xdigi",
"00154100_00000057_1.xdigi",
"00154100_00000058_1.xdigi",
"00154100_00000060_1.xdigi",
"00154100_00000062_1.xdigi",
"00154100_00000063_1.xdigi",
"00154100_00000064_1.xdigi",
"00154100_00000066_1.xdigi",
"00154100_00000068_1.xdigi",
"00154100_00000070_1.xdigi",
"00154100_00000071_1.xdigi",
"00154100_00000073_1.xdigi",
"00154100_00000076_1.xdigi",
"00154100_00000077_1.xdigi",
"00154100_00000088_1.xdigi",
"00154100_00000089_1.xdigi",
"00154100_00000090_1.xdigi",
"00154100_00000091_1.xdigi",
"00154100_00000092_1.xdigi",
"00154100_00000093_1.xdigi",
"00154100_00000096_1.xdigi",
"00154100_00000099_1.xdigi",

]
file_places = []
for file in files:
    file_place = "root:://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00154100/0000/" + file
    file_places.append( file_place )

options.input_files = file_places
options.input_type = "ROOT"
options.input_raw_format = 0.5

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'

# needed to run over FTv2 data
from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with allen_sequence.bind(sequence="hlt1_pp_veloSP"), reconstruction.bind( from_file=False ):
    run_chained_hlt_with_tuples( options, decay, public_tools=[stateProvider_with_simplified_geom()] )
"""
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, decay, public_tools=[stateProvider_with_simplified_geom()]) 
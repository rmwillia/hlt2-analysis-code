from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpResolvedPi0EE_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron, make_Kstps_with_pi0s
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    #### Nominal line
    Moore = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 9.,
            "min_cosine" : 0.9995,
        },
        "dielectrons": {
            "adocachi2cut_min": 9.,
            "ipchi2_e_min": 9.,
            "pid_e_min": 2.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 250. * MeV,
            "p_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 9.,
            "bpvvdchi2_min":16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "Kstps": {
            "PIDK_K_min": 1.,
            "p_K_min": 1_000. * MeV,
            "am_min": 600. * MeV,
            "am_max": 2_500. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 500. * MeV,
            "min_ipchi2_k": 9.,
            "Kstp_max_vtxchi2": 36.,
            "pt_K_min": 2_000. * MeV,
            "pt_pi_min": 250. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dielectron.bind( **Moore["dielectrons"] ) , 
        make_Kstps_with_pi0s.bind( **Moore["Kstps"] ) , 
        make_rd_BToXll.bind( **Moore["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0EE_line(name="Hlt2RD_BuToKpResolvedPi0EE_Moore") )
    
    
    
    
    
    

    
    ### run2
    run2 = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 9.,
            "min_cosine" : 0.9995,
        },
        "dielectrons": {
            "adocachi2cut_min": 999., ### is actually max
            "ipchi2_e_min": 9.,
            "pid_e_min": -999.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 350. * MeV,
            "p_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 9.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min":16.,
        },
        "Kstps": {
            "PIDK_K_min": -999.,
            "p_K_min": 0. * MeV,
            "am_min": 600. * MeV,
            "am_max": 2_500. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 400. * MeV,
            "min_ipchi2_k": 9.,
            "Kstp_max_vtxchi2": 999.,
            "pt_K_min": 400. * MeV,
            "pt_pi_min": 600. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dielectron.bind( **run2["dielectrons"] ) , 
        make_Kstps_with_pi0s.bind( **run2["Kstps"] ) , 
        make_rd_BToXll.bind( **run2["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0EE_line(name="Hlt2RD_BuToKpResolvedPi0EE_run2") )
    
    
    
    
    
    
    ### run2 and pid
    run2_and_pid = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 9.,
            "min_cosine" : 0.9995,
        },
        "dielectrons": {
            "adocachi2cut_min": 36., ### is actually max
            "ipchi2_e_min": 9.,
            "pid_e_min": 2.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 350. * MeV,
            "p_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 9.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min":16.,
        },
        "Kstps": {
            "PIDK_K_min": 1.,
            "p_K_min": 0. * MeV,
            "am_min": 600. * MeV,
            "am_max": 2_500. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 400. * MeV,
            "min_ipchi2_k": 9.,
            "Kstp_max_vtxchi2": 999.,
            "pt_K_min": 400. * MeV,
            "pt_pi_min": 600. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dielectron.bind( **run2_and_pid["dielectrons"] ) , 
        make_Kstps_with_pi0s.bind( **run2_and_pid["Kstps"] ) , 
        make_rd_BToXll.bind( **run2_and_pid["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0EE_line(name="Hlt2RD_BuToKpResolvedPi0EE_run2_and_pid") )
    
    
    
    
    
    """
    ### loose
    loose = {
        "B": {
            "am_min":0. * MeV,
            "am_max":100_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 0.,
            "vchi2pdof_max": 999.,
            "bpvipchi2_max": 999.,
            "min_cosine" : 0.,
        },
        "dielectrons": {
            "adocachi2cut_min": 999.,
            "ipchi2_e_min": 0.,
            "pid_e_min": -999.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 999.,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
        },
        "Kstps": {
            "PIDK_K_min": -999.,
            "p_K_min": 0. * MeV,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 0. * MeV,
            "min_ipchi2_k": 0.,
            "Kstp_max_vtxchi2": 999.,
            "pt_K_min": 0. * MeV,
            "pt_pi_min": 0. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dielectron.bind( **loose["dielectrons"] ) , 
        make_Kstps_with_pi0s.bind( **loose["Kstps"] ) , 
        make_rd_BToXll.bind( **loose["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0EE_line(name="Hlt2RD_BuToKpResolvedPi0EE_loose") )
     """   
    
    
    from copy import deepcopy as copy
    import pandas as pd
    
    _dict = copy( Moore )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( 
            f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpResolvedPi0EE_Moore__{particle}_.csv"
        )
    
    _dict = copy( run2 )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( 
            f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpResolvedPi0EE_run2__{particle}_.csv"
        )
    
    _dict = copy( run2_and_pid )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( 
            f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpResolvedPi0EE_run2+__{particle}_.csv"
        )
        
        
    
    
    
        
    
    
    
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/BuToKpResolvedPi0EE"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 50_000













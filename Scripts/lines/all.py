from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import all_lines



def make_lines():
    return [builder() for builder in all_lines.values()]
ntuple_address="/usera/rmw68/HLT2_lines_V2/pile/all"



options.lines_maker = make_lines
options.ntuple_file = ntuple_address

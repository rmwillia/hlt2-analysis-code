from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpPipPimMuMu_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dimuon, make_rd_detached_K1
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    #### Nominal line
    Moore = {
        "B": {
            "am_min": 4_500. * MeV,
            "am_max": 7_000. * MeV,
            "B_pt_min": 0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 16.,
            "bpvipchi2_max": 16.,
            "min_cosine": 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 25.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 250. * MeV,
            "p_muon_min": 3000. * MeV,
            "vchi2pdof_max": 25.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min": 16.,
        },
        "K1s": {
            "pi_PIDK_max": 4.,
            "K_PIDK_min": -4.,
            "K_1_pt_min": 250. * MeV,
            "pi_p_min": 1_000. * MeV,
            "pi_pt_min": 250. * MeV,
            "pi_ipchi2_min": 9.,
            "K_p_min": 1_000. * MeV,
            "K_pt_min": 250. * MeV,
            "K_ipchi2_min": 9.,
            "vchi2pdof_max": 9.,
            "adocachi2_max": 25.,
            "am_min": 500. * MeV,
            "am_max": 4200. * MeV,
            "K_1_min_DIRA" : None,
            "K_1_min_bpvfdchi2" : 0.,
        },
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **Moore["dimuons"] ) , 
        make_rd_detached_K1.bind( **Moore["K1s"] ) , 
        make_rd_BToXll.bind( **Moore["B"] ) 
    ):
        
        lines.append( BuToKpPipPimMuMu_line(name="Hlt2RD_BuToKpPipPimMuMu_Moore") )
    """
    #### run2
    run2 = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 999.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -999.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 350. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 9.,
            "bpvvdchi2_min": 16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "K1s": {
            "pi_PIDK_max": 999.,
            "K_PIDK_min": -999.,
            "K_1_pt_min": 0. * MeV,
            "pi_p_min": 1_000. * MeV,
            "pi_pt_min": 0. * MeV,
            "pi_ipchi2_min": 0.,
            "K_p_min": 1_000. * MeV,
            "K_pt_min": 0. * MeV,
            "K_ipchi2_min": 0.,
            "vchi2pdof_max": 12.,
            "adocachi2_max": 999.,
            "am_min": 0. * MeV,
            "am_max": 4200. * MeV,
            "K_1_min_DIRA" : None,
            "K_1_min_bpvfdchi2" : 0.,
        },
    }
    """
    with ( 
        make_rd_detached_dimuon.bind( **run2["dimuons"] ) , 
        make_rd_detached_K1.bind( **run2["K1s"] ) , 
        make_rd_BToXll.bind( **run2["B"] ) 
    ):
        
        lines.append( BuToKpPipPimMuMu_line(name="Hlt2RD_BuToKpPipPimMuMu_run2") )
    
    #### run2 + pid
    run2_and_pid = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 36.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 350. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 9.,
            "bpvvdchi2_min": 16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "K1s": {
            "pi_PIDK_max": 4.,
            "K_PIDK_min": -4.,
            "K_1_pt_min": 0. * MeV,
            "pi_p_min": 1_000. * MeV,
            "pi_pt_min": 0. * MeV,
            "pi_ipchi2_min": 9.,
            "K_p_min": 1_000. * MeV,
            "K_pt_min": 0. * MeV,
            "K_ipchi2_min": 9.,
            "vchi2pdof_max": 12.,
            "adocachi2_max": 36.,
            "am_min": 0. * MeV,
            "am_max": 4200. * MeV,
            "K_1_min_DIRA" : None,
            "K_1_min_bpvfdchi2" : 0.,
        },
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **run2_and_pid["dimuons"] ) , 
        make_rd_detached_K1.bind( **run2_and_pid["K1s"] ) , 
        make_rd_BToXll.bind( **run2_and_pid["B"] ) 
    ):
        
        lines.append( BuToKpPipPimMuMu_line(name="Hlt2RD_BuToKpPipPimMuMu_run2_and_pid") )
    
    
    from copy import deepcopy as copy
    import pandas as pd
    
    _dict = copy( Moore )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpPipPimEE_Moore__{particle}_.csv",index=False)
    
    _dict = copy( run2 )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpPipPimEE_run2__{particle}_.csv",index=False)
    
    _dict = copy( run2_and_pid )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/BuToKpPipPimEE_run2+__{particle}_.csv",index=False)
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/BuToKpPipPimMuMu"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 50_000













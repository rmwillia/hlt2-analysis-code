from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpResolvedPi0MuMu_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dimuon, make_Kstps_with_pi0s
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
        
    Base = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 36.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 350. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 9.,
            "bpvvdchi2_min": 16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "Kstps": {
            "PIDK_K_min": 1.,
            "p_K_min": 0. * MeV,
            "am_min": 600. * MeV,
            "am_max": 2_500. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 400. * MeV,
            "min_ipchi2_k": 9.,
            "Kstp_max_vtxchi2": 36.,
            "pt_K_min": 400. * MeV,
            "pt_pi_min": 600. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **Base["dimuons"] ) ,
        make_Kstps_with_pi0s.bind( **Base["Kstps"] ) , 
        make_rd_BToXll.bind( **Base["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0MuMu_line(name="Hlt2RD_BuToKpResolvedPi0MuMu_Base") )
    
    
    
    
    
    
    Vary = {
        "B": {
            "B_pt_min":[ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "FDchi2_min": [ 36. , 49., 64., 81., 100., 121., 144., 169.,],
            "vchi2pdof_max": [ 4., 9., 16., 25., 36.,],
            "bpvipchi2_max": [ 4., 9., 16., 25., 36.,],
        },
        "dimuons": {
            "adocachi2cut_max": [ 4., 9., 16., 25., 36.,],
            "ipchi2_muon_min": [ 4., 9., 16., 25., 36.,],
            "pidmu_muon_min": [ -4., -3., -2., -1., 0., 1., 2., 3., 4.],
            "pt_dimuon_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "pt_muon_min": [ 0. * MeV, 350. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "p_muon_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "vchi2pdof_max": [ 4., 9., 16., 25., 36.,],
            "bpvvdchi2_min": [ 16., 25., 36. , 49., 64., 81., 100., 121., 144., 169.,],
        },
        "Kstps": {
            "PIDK_K_min": [ -4., -3., -2., -1., 0., 1., 2., 3., 4.],
            "p_K_min": [ 0. * MeV, 350. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "Kstp_PT": [ 0. * MeV, 400. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "min_ipchi2_k": [ 4., 9., 16., 25., 36.,],
            "Kstp_max_vtxchi2": [ 4., 9., 16., 25., 36.,],
            "pt_K_min":  [ 0. * MeV, 400. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "pt_pi_min": [ 0. * MeV, 500. * MeV, 600. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
        }
    }
    
    from copy import deepcopy as copy
    import json
    base = {}

    for builder in Vary.keys():
        base[builder] = {}
        for variable in Vary[builder].keys():
            base[builder][variable] = str(Base[builder][variable])
            for selection in Vary[builder][variable]:
                
                if selection != Base[builder][variable]:

                    Args = copy(Base)
                    Args[builder][variable] = selection

                    if selection < 0:
                        selection_label = f"minus{abs(selection)}"
                    else:
                        selection_label = str( selection )

                    name = f"Hlt2RD_BuToKpResolvedPi0MuMu_Varying_builder_{builder}_variable_{variable}_selection_{selection_label}_"


                    with ( 
                        make_rd_detached_dimuon.bind( **Args["dimuons"] ) , 
                        make_Kstps_with_pi0s.bind( **Args["Kstps"] ) , 
                        make_rd_BToXll.bind( **Args["B"] ) 
                    ):
                        lines.append( BuToKpResolvedPi0MuMu_line(name=name) )
    
    with open("/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/jsons/Vary/BuToKpResolvedPi0MuMu_Base.json", "w") as outfile:
        outfile.write( json.dumps(base, indent=4) )
        
    
    
    
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/BuToKpResolvedPi0MuMu_Vary"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = -1













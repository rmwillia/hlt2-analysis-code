from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import B0ToKpKmEE_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron, make_rd_detached_phis
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    Base = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dielectrons": {
            "adocachi2cut_min": 36., ### is actually max
            "ipchi2_e_min": 9.,
            "pid_e_min": 2.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 350. * MeV,
            "p_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 9.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min":16.,
        },
        "phis": {
            "k_pid": (F.PID_K > -4.),
            "am_min": 0. * MeV,
            "am_max": 1100. * MeV,
            "k_p_min": 0. * MeV,
            "k_pt_min": 0. * MeV,
            "k_ipchi2_min": 4.,
            "phi_pt_min": 0. * MeV,
            "adocachi2cut": 30.,
            "vchi2pdof_max": 25.,
        }
    }
    
    with ( 
         make_rd_detached_dielectron.bind( **Base["dielectrons"] ) , 
        make_rd_detached_phis.bind( **Base["phis"] ) , 
        make_rd_BToXll.bind( **Base["B"] ) 
    ):
        
        lines.append( B0ToKpKmEE_line(name="Hlt2RD_B0ToKpKmEE_Base") )
    
    
    
    
    
    
    Vary = {
        "B": {
            "B_pt_min":[ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "FDchi2_min": [ 36. , 49., 64., 81., 100., 121., 144., 169.,],
            "vchi2pdof_max": [ 4., 9., 16., 25., 36.,],
            "bpvipchi2_max": [ 4., 9., 16., 25., 36.,],
        },
        "dielectrons": {
            "adocachi2cut_min": [ 4., 9., 16., 25., 36.,], ### is actually max
            "ipchi2_e_min": [ 4., 9., 16., 25., 36.,],
            "pid_e_min": [ -4., -3., -2., -1., 0., 1., 2., 3., 4.],
            "pt_diE_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "pt_e_min": [ 0. * MeV, 350. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "p_e_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,], 
            "vfaspfchi2ndof_max": [ 4., 9., 16., 25., 36.,],
            "bpvvdchi2_min":[ 16., 25., 36. , 49., 64., 81., 100., 121., 144., 169.,],
        },
        "phis": {
            "am_max": [ 1_100. * MeV, 1_200. * MeV, 1_500. * MeV, ],
            "k_p_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "k_pt_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "k_ipchi2_min": [ 4., 9., 16., 25., 36.,],
            "phi_pt_min": [ 0. * MeV, 500. * MeV, 1_000. * MeV, 2_000. * MeV, 5_000. * MeV,],
            "adocachi2cut": [ 4., 9., 16., 25., 30., 36.,],
            "vchi2pdof_max": [ 4., 9., 16., 25., 36.,],
        }
    }
    
    from copy import deepcopy as copy
    import json
    base = {}

    for builder in Vary.keys():
        base[builder] = {}
        for variable in Vary[builder].keys():
            base[builder][variable] = str(Base[builder][variable])
            for selection in Vary[builder][variable]:
                
                if selection != Base[builder][variable]:

                    Args = copy(Base)
                    Args["phis"]["k_pid"] = (F.PID_K > -4.)

                    Args[builder][variable] = selection

                    if selection < 0:
                        selection_label = f"minus{abs(selection)}"
                    else:
                        selection_label = str( selection )

                    name = f"Hlt2RD_B0ToKpKmEE_Varying_builder_{builder}_variable_{variable}_selection_{selection_label}_"


                    with ( 
                        make_rd_detached_dielectron.bind( **Args["dielectrons"] ) , 
                        make_rd_detached_phis.bind( **Args["phis"] ) , 
                        make_rd_BToXll.bind( **Args["B"] ) 
                    ):
                        lines.append( B0ToKpKmEE_line(name=name) )
    
    with open("/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/jsons/Vary/B0ToKpKmEE_Base.json", "w") as outfile:
        outfile.write( json.dumps(base, indent=4) )
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/B0ToKpKmEE_Vary"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = -1













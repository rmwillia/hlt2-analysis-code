from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import B0ToKpPimMuMu_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dimuon, make_rd_detached_kaons, make_rd_detached_pions, make_rd_detached_kstar0s
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    #### Nominal line
    Moore = {
        "B": {
            "am_min": 4_500. * MeV,
            "am_max": 7_000. * MeV,
            "B_pt_min": 0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 25.,
            "bpvipchi2_max": 25.,
            "min_cosine": 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 25.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 250. * MeV,
            "p_muon_min": 3000. * MeV,
            "vchi2pdof_max": 9.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min": 16.,
        },
        "Kst0s": {
            "am_min": 600. * MeV,
            "am_max": 2_500. * MeV,
            #"pi_pid": (F.PID_K < 4.),
            #"k_pid": (F.PID_K > -4.),
            "pi_p_min": 1_000. * MeV,
            "pi_pt_min": 250. * MeV,
            "pi_ipchi2_min": 9.,
            "k_p_min": 1_000. * MeV,
            "k_pt_min": 250. * MeV,
            "k_ipchi2_min": 9.,
            "kstar0_pt_min": 250. * MeV,
            "adocachi2cut": 25.,
            "vchi2pdof_max": 25.,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **Moore["dimuons"] ) , 
        make_rd_detached_kstar0s.bind( **Moore["Kst0s"] ) , 
        make_rd_BToXll.bind( **Moore["B"] ) 
    ):
        
        lines.append( B0ToKpPimMuMu_line(name="Hlt2RD_B0ToKpPimMuMu_Moore") )
    
    
    
    
    
    
    
    #### run2 line
    run2 = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 999.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -999.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 350. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 9.,
            "bpvvdchi2_min": 16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "Kst0s": {
            "am_min": 0. * MeV,
            "am_max": 2_600. * MeV,
            "pi_pid": (F.PID_K < 999.),
            "k_pid": (F.PID_K > -999.),
            "pi_p_min": 2_000. * MeV,
            "pi_pt_min": 250. * MeV,
            "pi_ipchi2_min": 9.,
            "k_p_min": 1_000. * MeV,
            "k_pt_min": 250. * MeV,
            "k_ipchi2_min": 9.,
            "kstar0_pt_min": 400. * MeV,
            "adocachi2cut": 30.,
            "vchi2pdof_max": 25.,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **run2["dimuons"] ) , 
        make_rd_detached_kstar0s.bind( **run2["Kst0s"] ) , 
        make_rd_BToXll.bind( **run2["B"] ) 
    ):
        
        lines.append( B0ToKpPimMuMu_line(name="Hlt2RD_B0ToKpPimEE_run2") )
    
    
    
    
    
    
    
    #### run2 + pid line
    run2_and_pid = {
        "B": {
            "am_min":4_500. * MeV,
            "am_max":7_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 100.,
            "vchi2pdof_max": 9.,
            "bpvipchi2_max": 25.,
            "min_cosine" : 0.9995,
        },
        "dimuons": {
            "adocachi2cut_max": 36.,
            "ipchi2_muon_min": 9.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 350. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 9.,
            "bpvvdchi2_min": 16.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
        },
        "Kst0s": {
            "am_min": 0. * MeV,
            "am_max": 2_600. * MeV,
            "pi_pid": (F.PID_K < 4.),
            "k_pid": (F.PID_K > -4.),
            "pi_p_min": 2_000. * MeV,
            "pi_pt_min": 250. * MeV,
            "pi_ipchi2_min": 9.,
            "k_p_min": 1_000. * MeV,
            "k_pt_min": 250. * MeV,
            "k_ipchi2_min": 9.,
            "kstar0_pt_min": 400. * MeV,
            "adocachi2cut": 30.,
            "vchi2pdof_max": 25.,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **run2_and_pid["dimuons"] ) , 
        make_rd_detached_kstar0s.bind( **run2_and_pid["Kst0s"] ) , 
        make_rd_BToXll.bind( **run2_and_pid["B"] ) 
    ):
        
        lines.append( B0ToKpPimMuMu_line(name="Hlt2RD_B0ToKpPimEE_run2_and_pid") )
    
    
    
    
    
    
    """
    #### loose line
    loose = {
        "B": {
            "am_min":0. * MeV,
            "am_max":100_000. * MeV,
            "B_pt_min":0. * MeV,
            "FDchi2_min": 0.,
            "vchi2pdof_max": 999.,
            "bpvipchi2_max": 999.,
            "min_cosine" : 0.,
        },
        "dimuons": {
            "adocachi2cut_max": 999.,
            "ipchi2_muon_min": 0.,
            "pidmu_muon_min": -999.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 0. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 999.,
            "bpvvdchi2_min": 0.,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
        },
        "Kst0s": {
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "pi_pid": (F.PID_K < 999.),
            "k_pid": (F.PID_K > -999.),
            "pi_p_min": 0. * MeV,
            "pi_pt_min": 0. * MeV,
            "pi_ipchi2_min": 0.,
            "k_p_min": 0. * MeV,
            "k_pt_min": 0. * MeV,
            "k_ipchi2_min": 0.,
            "kstar0_pt_min": 0. * MeV,
            "adocachi2cut": 0.,
            "vchi2pdof_max": 999.,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **loose["dimuons"] ) , 
        make_rd_detached_kstar0s.bind( **loose["Kst0s"] ) , 
        make_rd_BToXll.bind( **loose["B"] ) 
    ):
        
        lines.append( B0ToKpPimMuMu_line(name="Hlt2RD_B0ToKpPimEE_loose") )
    """
    
    
    from copy import deepcopy as copy
    import pandas as pd
    
    _dict = copy( Moore )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/B0ToKpPimMuMu_Moore__{particle}_.csv",index=False)
    
    _dict = copy( run2 )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/B0ToKpPimMuMu_run2__{particle}_.csv",index=False)
    
    _dict = copy( run2_and_pid )
    for particle in _dict.keys():
        for variable in _dict[particle].keys():
            _dict[particle][variable] = [str( _dict[particle][variable] )]
        df = pd.DataFrame.from_dict( _dict[particle] )
        df.to_csv( f"/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Outputs/Selection_DataFrames/B0ToKpPimMuMu_run2+__{particle}_.csv",index=False)    
        
        
        
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/B0ToKpPimMuMu"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 50_000













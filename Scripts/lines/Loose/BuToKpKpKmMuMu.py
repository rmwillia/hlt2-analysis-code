from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpKpKmMuMu_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dimuon, make_rd_detached_K2
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
      
    Loose = {
        "B": {
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "B_pt_min": 0. * MeV,
            "FDchi2_min": 0.,
            "vchi2pdof_max": 999.,
            "bpvipchi2_max": 999.,
            "min_cosine": 0.,
        },
        "dimuons": {
            "adocachi2cut_max": 25.,
            "ipchi2_muon_min": 4.,
            "pidmu_muon_min": -4.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 250. * MeV,
            "p_muon_min": 3000. * MeV,
            "vchi2pdof_max": 25.,
            "am_min": 0. * MeV,
            "am_max": 5_500. * MeV,
            "bpvvdchi2_min": 16.,
        },
        "K2s": {
            "adocachi2_max": 999.,
            "am_min":0. * MeV,
            "am_max":100_000. * MeV,
            "K_PIDK_min": -999.,
            "K_p_min": 0. * MeV,
            "K_pt_min": 0. * MeV,
            "K_ipchi2_min": 0.,
            "K_2_pt_min": 0. * MeV,
            "vchi2pdof_max": 999.,
            "K_2_min_bpvfdchi2": 0.,
        },
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **Loose["dimuons"] ), 
        make_rd_detached_K2.bind( **Loose["K2s"] ) , 
        make_rd_BToXll.bind( **Loose["B"] ) 
    ):
        
        lines.append( BuToKpKpKmMuMu_line(name="Hlt2RD_BuToKpKpKmEE_Loose") )
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/BuToKpKpKmMuMu_Loose"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 20_000













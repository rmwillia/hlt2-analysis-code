from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpResolvedPi0MuMu_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dimuon, make_Kstps_with_pi0s
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    #### Nominal line
    Loose = {
        "B": {
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "B_pt_min": 0. * MeV,
            "FDchi2_min": 0.,
            "vchi2pdof_max": 999.,
            "bpvipchi2_max": 999.,
            "min_cosine": 0.,
        },
        "dimuons": {
            "adocachi2cut_max": 999.,
            "ipchi2_muon_min": 0.,
            "pidmu_muon_min": -999.,
            "pt_dimuon_min": 0. * MeV,
            "pt_muon_min": 0. * MeV,
            "p_muon_min": 0. * MeV,
            "vchi2pdof_max": 999.,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "bpvvdchi2_min": 0.,
        },
        "Kstps": {
            "PIDK_K_min": 999.,
            "p_K_min": 0. * MeV,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "pi0_type": "resolved",
            "Kstp_PT": 0. * MeV,
            "min_ipchi2_k": 0.,
            "Kstp_max_vtxchi2": 999.,
            "pt_K_min": 0. * MeV,
            "pt_pi_min": 0. * MeV,
        }
    }
    
    with ( 
        make_rd_detached_dimuon.bind( **Loose["dimuons"] ), 
        make_Kstps_with_pi0s.bind( **Loose["Kstps"] ) , 
        make_rd_BToXll.bind( **Loose["B"] ) 
    ):
        
        lines.append( BuToKpResolvedPi0MuMu_line(name="Hlt2RD_BuToKpResolvedPi0MuMu_Loose") )
    
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/BuToKpResolvedPi0MuMu_Loose"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 20_000













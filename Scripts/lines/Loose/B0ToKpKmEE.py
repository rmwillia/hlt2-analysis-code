from Moore import options
import os

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import B0ToKpKmEE_line 
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron, make_rd_detached_phis
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
import Functors as F
from GaudiKernel.SystemOfUnits import MeV

def make_lines():    
    
    lines = []
    
    Loose = {
        "B": {
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "B_pt_min": 0. * MeV,
            "FDchi2_min": 0.,
            "vchi2pdof_max": 999.,
            "bpvipchi2_max": 999.,
            "min_cosine": 0.,
        },
        "dielectrons": {
            "adocachi2cut_min": 999.,
            "ipchi2_e_min": 0.,
            "pid_e_min": -999.,
            "pt_diE_min": 0. * MeV,
            "pt_e_min": 0. * MeV,
            "p_e_min": 0. * MeV,
            "vfaspfchi2ndof_max": 999.,
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "bpvvdchi2_min": 0.,
        },
        "phis": {
            "k_pid": (F.PID_K > -999.),
            "am_min": 0. * MeV,
            "am_max": 100_000. * MeV,
            "k_p_min": 0. * MeV,
            "k_pt_min": 0. * MeV,
            "k_ipchi2_min": 0.,
            "phi_pt_min": 0. * MeV,
            "adocachi2cut": 999.,
            "vchi2pdof_max": 999.,
        }
    }
    
    with ( 
        make_rd_detached_dielectron.bind( **Loose["dielectrons"] ) , 
        make_rd_detached_phis.bind( **Loose["phis"] ) , 
        make_rd_BToXll.bind( **Loose["B"] ) 
    ):
        
        lines.append( B0ToKpKmEE_line(name="Hlt2RD_B0ToKpKmEE_Loose") )
    
    
    return lines

ntuple_address="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/RootDIR/B0ToKpKmEE_Loose"
options.lines_maker = make_lines
options.ntuple_file = ntuple_address
options.evt_max = 20_000













#!/bin/bash

cd /usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Condor/

job_DIR="/usera/rmw68/HLT2_lines_V2/hlt2-analysis-code/Condor/jobs/"

rm -r err log out
mkdir -p err log out




### B+ -> K+ e+ e-
condor_submit ${job_DIR}BuToKpEE_eff.job
condor_submit ${job_DIR}BuToKpEE_rate.job
### B+ -> K+ mu+ mu-
condor_submit ${job_DIR}BuToKpMuMu_eff.job
condor_submit ${job_DIR}BuToKpMuMu_rate.job



### B+ -> pi+ e+ e-
condor_submit ${job_DIR}BuToPipEE_eff.job
condor_submit ${job_DIR}BuToPipEE_rate.job
### B+ -> pi+ mu+ mu-
condor_submit ${job_DIR}BuToPipMuMu_eff.job
condor_submit ${job_DIR}BuToPipMuMu_rate.job



### B+ -> K+ pi0 e+ e-
condor_submit ${job_DIR}BuToKpResolvedPi0EE_rate.job
### B+ -> K+ pi0 mu+ mu-
condor_submit ${job_DIR}BuToKpResolvedPi0MuMu_eff.job
condor_submit ${job_DIR}BuToKpResolvedPi0MuMu_rate.job



### B0 -> K+ K- e+ e-
condor_submit ${job_DIR}B0ToKpKmEE_eff.job
condor_submit ${job_DIR}B0ToKpKmEE_rate.job
### B0 -> K+ K- mu+ mu-
condor_submit ${job_DIR}B0ToKpKmMuMu_eff.job
condor_submit ${job_DIR}B0ToKpKmMuMu_rate.job



### B0 -> K+ pi- e+ e-
condor_submit ${job_DIR}B0ToKpPimEE_eff.job
condor_submit ${job_DIR}B0ToKpPimEE_rate.job
### B0 -> K+ pi- mu+ mu-
condor_submit ${job_DIR}B0ToKpPimMuMu_eff.job
condor_submit ${job_DIR}B0ToKpPimMuMu_rate.job



### B0 -> pi+ pi- e+ e-
condor_submit ${job_DIR}B0ToPipPimEE_rate.job
### B0 -> pi+ pi- mu+ mu-
condor_submit ${job_DIR}B0ToPipPimMuMu_eff.job
condor_submit ${job_DIR}B0ToPipPimMuMu_rate.job



### B+ -> K+ pi+ pi- e+ e-
condor_submit ${job_DIR}BuToKpPipPimEE_eff.job
condor_submit ${job_DIR}BuToKpPipPimEE_rate.job
### B+ -> K+ pi+ pi- mu+ mu-
condor_submit ${job_DIR}BuToKpPipPimMuMu_eff.job
condor_submit ${job_DIR}BuToKpPipPimMuMu_rate.job



### B+ -> K+ K+ K- e+ e-
condor_submit ${job_DIR}BuToKpKpKmEE_rate.job
### B+ -> K+ K+ K- mu+ mu-
condor_submit ${job_DIR}BuToKpKpKmMuMu_rate.job




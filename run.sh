#!/bin/bash

HELP_MESSAGE="
Usage: scriptname [options]

Options:
-h, --help    Show this help message and exit
positional arg 1 -> line(s) to run over, options include: 
    BuToKpEE , 
    BuToKpMuMu ,
    BuToPipEE ,
    BuToPipMuMu ,
    BuToKpResolvedPi0EE (no simulation),
    BuToKpResolvedPi0MuMu ,
    B0ToPipPimEE  (no simulation),
    B0ToPipPimMuMu ,
    B0ToKpPimEE ,
    B0ToKpPimMuMu ,
    B0ToKpKmEE ,
    B0ToKpKmMuMu ,
    BuToKpPipPimEE ,
    BuToKpPipPimMuMu ,
    BuToKpKpKmEE  (no simulation),
    BuToKpKpKmMuMu  (no simulation),
 or All
positional arg 2 -> wether to find efficiency or rate: eff or rate

Example run: 
 - source run.sh BuToKpEE eff
 - source run.sh BuToKpEE rate
 - source run.sh All rate


Note: cannot run ' source run.sh All eff ' or run 'eff' for modes without simulation.
"

if [[ $1 == "--help" || $1 == "-h" ]]; then
    echo "$HELP_MESSAGE"
    exit 0
fi








######################################

#             Important DIRs

######################################

LINE=$1 ### Which line to run ( specific or all )
TYPE=$2 ### pass "eff" if want to find efficiency, pass "rate" if want to find rate

### directory containg stack and scripts
DIR="${HOME}/HLT2_lines_V2/hlt2-analysis-code"
stackDIR="${HOME}/HLT2_lines_V2/stack"






### directory where root files are stored
RootDIR="${DIR}/RootDIR"

### directory to place output
OUT="${DIR}/Outputs"

### directory containg scripts
scripts_dir="${DIR}/Scripts"

### line options
line_options="${scripts_dir}/lines/${LINE}.py"

### run command
run="${stackDIR}/MooreAnalysis/run"
cd $stackDIR















########################################

#         Check if valid run type      #

########################################

if [ $TYPE = "eff" ]; then
 echo "For ${LINE} getting efficiency"
elif [ $TYPE = "rate" ]; then
    echo "For ${LINE} getting rate"
else 
    echo "second positional arg must be either 'eff' or 'rate', you gave: ${TYPE}"
    exit 1
fi








############################################################

#         Get options for running over specified line      #

############################################################

if [ $LINE = 'all' ]; then
    echo "running over all lines (only for rate calculation)"

elif [ $LINE = 'BuToKpEE' ]; then
    For_Eff="--reconstructible-children=ep,em,Kp --true-signal-to-match-to=Bp --parent=Bp"
    
elif [ $LINE = 'BuToKpMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,Kp --true-signal-to-match-to=Bp --parent=Bp"

elif [ $LINE = 'BuToPipEE' ]; then
    For_Eff="--reconstructible-children=ep,em,pip --true-signal-to-match-to=Bp --parent=Bp"
    
elif [ $LINE = 'BuToPipMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,pip --true-signal-to-match-to=Bp --parent=Bp"

elif [ $LINE = 'B0ToKpPimEE' ]; then
    For_Eff="--reconstructible-children=ep,em,Kp,Pim --true-signal-to-match-to=Bd --parent=Bd"
    
elif [ $LINE = 'B0ToKpPimMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,Kp,Pim --true-signal-to-match-to=Bd --parent=Bd"

elif [ $LINE = 'B0ToKpKmEE' ]; then
    For_Eff="--reconstructible-children=ep,em,Kp,Km --true-signal-to-match-to=Bs --parent=Bs"
    
elif [ $LINE = 'B0ToKpKmMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,Kp,Km --true-signal-to-match-to=Bs --parent=Bs"
    
elif [ $LINE = 'B0ToPipPimEE' ]; then
    For_Eff="None"
    
elif [ $LINE = 'B0ToPipPimMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,pip,pim --true-signal-to-match-to=B0 --parent=B0"  

elif [ $LINE = 'BuToKpResolvedPi0EE' ]; then
    For_Eff="None"
    
elif [ $LINE = 'BuToKpResolvedPi0MuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,Kp --true-signal-to-match-to=Bp --parent=Bp"  

elif [ $LINE = 'BuToKpPipPimEE' ]; then
    For_Eff="--reconstructible-children=ep,em,Kp,pim,pip --true-signal-to-match-to=Bu --parent=Bu"
    
elif [ $LINE = 'BuToKpPipPimMuMu' ]; then
    For_Eff="--reconstructible-children=mup,mum,Kp,pim,pip --true-signal-to-match-to=Bu --parent=Bu"
    
elif [ $LINE = 'BuToKpKpKmEE' ]; then
    For_Eff="None"
    
elif [ $LINE = 'BuToKpKpKmMuMu' ]; then
    For_Eff="None"
    
    
    
else
    echo "LINE = ${LINE} not covered"
    exit 1
fi

























############################################################

#                         Run Rate                         #

############################################################

if [ $TYPE = "rate" ]
then    
    compute_rates="${stackDIR}/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py"
    rate_options="${scripts_dir}/rate/rate_options.py"
    minbias_sample="${scripts_dir}/rate/minbias_sample.py"
    ntuple="${RootDIR}/${LINE}_rate.root"  
    output_txt="${OUT}/${LINE}_rate_output.txt" 
    echo "Using line options ${line_options}"    
    
    echo "Begin run line over minbias from ${minbias_sample}"
    $run gaudirun.py $minbias_sample $line_options $rate_options
    
    echo "Begin compute rates"
    $run $compute_rates $ntuple --using-hlt1-filtered-MC --json="${OUT}/jsons/${LINE}_rate.json" 
fi










############################################################

#                         Run Eff                          #

############################################################

if [ $TYPE = "eff" ]
then
    compute_effs="${stackDIR}/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py"
    line_options="${scripts_dir}/lines/${LINE}.py"
    eff_options="${scripts_dir}/Simulation/${LINE}.py"
    ntuple="${RootDIR}/${LINE}_eff.root"  
    output_txt="${OUT}/${LINE}_eff_output.txt" 
    echo "Using line options ${line_options}"
        
    echo "Begin run line over simulation from ${eff_options}"
    $run gaudirun.py $line_options $eff_options
    
    echo "Compute efficiencies"
    $run $compute_effs $ntuple $For_Eff --json="${OUT}/jsons/${LINE}_CanRecoChildren_eff.json"  --denoms=CanRecoChildren
fi









cd $DIR
echo "Complete"

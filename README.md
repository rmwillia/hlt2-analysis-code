# HLT2 Analysis Code


## run

Key commands

``` bash
source run.sh LINE TYPE
```

LINE = BuToKpEE , BuToKpMuMu , or All
TYPE = rate, eff

note: cannot do ``` source run.sh ALL eff ``` as there is no 'All' sim

Examples of proper use:

``` bash
source run.sh BuToKpEE eff
```

``` bash
source run.sh BuToKpMuMu rate
```

``` bash
source run.sh All rate
```

## Change

You will need to change in ```run.sh``` the following directories to match your directory

```DIR``` is the directory of the git repo 
``` stackDIR ``` is the directory of the lhcb stack --- see setup [here](https://gitlab.cern.ch/rmatev/lb-stack-setup)


``` bash
### directory containg stack and scripts
DIR="${HOME}/HLT2_lines_V2/hlt2-analysis-code"
stackDIR="${HOME}/HLT2_lines_V2/stack"
``` 


You will need to change is what is in ```./Scripts/```

 - The directory to place output root files. ``` ntuple_address="/usera/rmw68/HLT2_lines_V2/pile/BuToKpEE" ``` to whatever matches local.

Line files within allow you to change the selections & the number of events you run over.
